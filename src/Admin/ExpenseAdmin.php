<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: ExpenceAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/ExpenceAdmin.php
 * project: Admigrua2
 * File: ExpenceAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;

class ExpenseAdmin  extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('id')
            ->add('amount')
            ->add('date', DateTimePickerType::class)
            ->add('description')
            ->add('timeStamp', DateTimePickerType::class)
            ->add('groupCode')
            ->add('userCode')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('amount')
            ->add('date')
            ->add('description')
            ->add('timeStamp')
            ->add('groupCode')
            ->add('userCode')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('amount')
            ->add('date')
            ->add('description')
            ->add('timeStamp')
            ->add('groupCode')
            ->add('userCode')
        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id')
            ->add('amount')
            ->add('date')
            ->add('description')
            ->add('timeStamp')
            ->add('groupCode')
            ->add('userCode')
        ;
    }

}