<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: AssistanceVehicleAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/AssistanceVehicleAdmin.php
 * project: Admigrua2
 * File: AssistanceVehicleAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;

class AssistanceVehicleAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            ->add('id')
            ->add('plate')
            ->add('alias')
            ->add('brand')
            ->add('model')
            ->add('version')
            ->add('photo')
            ->add('creationDate', DateTimePickerType::class,['format' => 'dd/MM/yyyy'])
            ->add('updatedDate', DateTimePickerType::class)
            ->add('active')
            ->add('typeAssistanceVehicle')
            ->add('userCode')
		;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('plate')
            ->add('alias')
            ->add('brand')
            ->add('model')
            ->add('version')
            ->add('photo')
            ->add('creationDate')
            ->add('updatedDate')
            ->add('active')
            ->add('typeAssistanceVehicle')
//            ->add('userCode')
		;
    }
    protected function configureShowFields(ShowMapper $showMapper){
        $showMapper
            ->add('id')
            ->add('plate')
            ->add('alias')
            ->add('brand')
            ->add('model')
            ->add('version')
            ->add('photo','string', ['template' => 'AssistanceVehicleAliasPhotoShow.html.twig'])
            ->add('creationDate')
            ->add('updatedDate')
            ->add('active')
            ->add('typeAssistanceVehicle','string', ['template' => 'AssistanceVehicleAliasIconShow.html.twig'])
            ->add('userCode')
        ;
    }

    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->addIdentifier('id')
            ->add('plate')
            ->add('alias')
            ->add('brand')
            ->add('model')
            ->add('version')
            ->add('photo')
            ->add('creationDate')
            ->add('updatedDate')
            ->add('active')
            ->add('typeAssistanceVehicle','string', ['template' => 'AssistanceVehicleAliasIcon.html.twig'])
            ->add('userCode')
            ->add('_action', null, [
                'actions' => [
                    'show'    => [],
                    'edit'    => [],
                    'delete'  => [],
                ]
            ])
        ;
    }
}