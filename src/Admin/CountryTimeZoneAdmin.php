<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/9/25
 * File: CountryTimeZone.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/CountryTimeZone.php
 * project: Admigrua2
 * File: CountryTimeZone.php
 * *********************************************************************
 */

/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: CountryAdminAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/CountryAdmin.phpn.php
 * project: Admigrua2
 * File: CountryAdminAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;

class CountryTimeZoneAdmin extends AbstractAdmin {

	protected function configureFormFields (FormMapper $formMapper) {
		$formMapper
			->add('countrycode')
			->add('name')
            ->add('countryhexadecimal')
            ->add('zonename')
		;
	}

	protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
		$datagridMapper
            ->add('countrycode')
            ->add('name')
            ->add('countryhexadecimal')
            ->add('zonename')
		;
	}

	protected function configureShowFields (ShowMapper $showMapper) {
		$showMapper
            ->add('countrycode')
            ->add('name')
            ->add('countryhexadecimal')
            ->add('zonename')
		;
	}

	protected function configureListFields (ListMapper $listMapper) {
		$listMapper
            ->add('countrycode')
            ->add('name')
            ->add('countryhexadecimal')
            ->add('zonename');
	}



}