<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: BrandAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/BrandAdmin.php
 * project: Admigrua2
 * File: BrandAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BrandAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('name', TextType::class)
            ->add('slug', TextType::class)
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('slug')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('slug')
        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id',null,['label'=>'Holi'])
            ->add('name',null,['label'=>'Nombres'])
            ->add('slug')
        ;
    }
}