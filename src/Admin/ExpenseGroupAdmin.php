<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: ExpenceGroupAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/ExpenceGroupAdmin.php
 * project: Admigrua2
 * File: ExpenceGroupAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ExpenseGroupAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper

            ->add('name')
            ->add('description')
            ->add('creationDate')
            ->add('updatedDate')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper

            ->add('name')
            ->add('description')
            ->add('creationDate')
            ->add('updatedDate')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper

            ->add('name')
            ->add('description')
            ->add('creationDate')
            ->add('updatedDate')
        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper

            ->add('name')
            ->add('description')
            ->add('creationDate')
            ->add('updatedDate')
        ;
    }
}