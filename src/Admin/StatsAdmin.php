<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/25
 * File: StatsAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/StatsAdmin.php
 * project: Admigrua2
 * File: StatsAdmin.php
 * *********************************************************************
 */

namespace App\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class StatsAdmin extends AbstractAdmin {
    protected $baseRoutePattern = 'stats';
    protected $baseRouteName = 'stats';

    protected function configureRoutes (RouteCollection $collection) {
        $collection->clearExcept(['list']);
    }


}