<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/16
 * File: IconAdminAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/IconAdminAdmin.php
 * project: Admigrua2
 * File: IconAdminAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class IconAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('id')
			->add('name')
            ->add('codehex')
            ->add('fontawesome')
		;
	}
	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('id')
			->add('name')
            ->add('codehex')
            ->add('fontawesome')
		;
	}
	public function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('id')
			->add('name')
            ->add('codehex')
            ->add('fontawesome')
		;
	}
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
			->add('name')
            ->add('codehex')
            ->add('fontawesome', 'string', ['template' => 'awesomeFontIcon.html.twig'])
		;
	}
}