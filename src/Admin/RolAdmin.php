<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/16
 * File: RolAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/RolAdmin.php
 * project: Admigrua2
 * File: RolAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RolAdmin  extends AbstractAdmin {

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('code')
			->add('name')
			->add('description')
			->add('acces')
		;
	}
	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('code')
			->add('name')
			->add('description')
			->add('acces')
		;
	}
	public function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('code')
			->add('name')
			->add('description')
			->add('acces')
		;
	}
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('code')
//			->add('icon')
			->add('name')
			->add('description')
			->add('acces')
		;
	}


}