<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: IncidenceAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/IncidenceAdmin.php
 * project: Admigrua2
 * File: IncidenceAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;

class IncidenceAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('expense')
            ->add('date',DatePickerType::class)
            ->add('description')
            ->add('timeStamp',DatePickerType::class)
            ->add('assistanceVehicle')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('expense')
            ->add('date')
            ->add('description')
            ->add('timeStamp')
            ->add('assistanceVehicle')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('expense')

        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('expense')
            ->add('date')
            ->add('description')
            ->add('timeStamp')
            ->add('assistanceVehicle')
        ;
    }
}