<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: SystemAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/SystemAdmin.php
 * project: Admigrua2
 * File: SystemAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SystemAdmin  extends AbstractAdmin {



	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper

			->add('imagePath')
			->add('filePath')
			->add('defaultprofileimage')
			->add('defaultasistancevehicleimage')
			->add('defaultlogoimage')
			->add('date')
			->add('defaultCountryCode')
		;
	}
	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('id')
			->add('imagePath')
			->add('filePath')
			->add('defaultprofileimage')
			->add('defaultasistancevehicleimage')
			->add('defaultlogoimage')
			->add('date')
			->add('defaultCountryCode')
		;
	}
	public function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('id')
			->add('imagePath')
			->add('filePath')
			->add('defaultprofileimage')
			->add('defaultasistancevehicleimage')
			->add('defaultlogoimage')
			->add('date')
			->add('defaultCountryCode')
		;
	}
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
			->add('imagePath')
			->add('filePath')
			->add('defaultprofileimage')
			->add('defaultasistancevehicleimage')
			->add('defaultlogoimage')
			->add('date')
			->add('defaultCountryCode')
		;
	}


}