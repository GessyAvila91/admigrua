<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: ZoneAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/ZoneAdmin.php
 * project: Admigrua2
 * File: ZoneAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DatePickerType;

class ZoneAdmin extends AbstractAdmin {

	protected function configureFormFields (FormMapper $formMapper) {
		$formMapper
			->add('id')
			->add('name')
			->add('countryCode')
		;
	}

	protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('id')
			->add('name')
			->add('countryCode')
		;
	}

	protected function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('id')
			->add('name')
			->add('countryCode')
		;
	}

	protected function configureListFields (ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
			->add('name')
			->add('countryCode')
		;
	}


}