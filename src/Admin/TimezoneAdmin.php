<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: TimezoneAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/TimezoneAdmin.php
 * project: Admigrua2
 * File: TimezoneAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TimezoneAdmin extends AbstractAdmin {

	protected function configureFormFields (FormMapper $formMapper) {
		$formMapper
			->add('abbreviartion')
			->add('timeStart')
			->add('gmtOffset')
			->add('dst')
		;
	}

	protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('abbreviartion')
			->add('timeStart')
			->add('gmtOffset')
			->add('dst')
		;
	}

	protected function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('abbreviartion')
			->add('timeStart')
			->add('gmtOffset')
			->add('dst')
		;
	}

	protected function configureListFields (ListMapper $listMapper) {
		$listMapper
			->addIdentifier('abbreviartion')
			->add('timeStart')
			->add('gmtOffset')
			->add('dst')
		;
	}

}