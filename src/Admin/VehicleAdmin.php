<?php

namespace App\Admin;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class VehicleAdmin extends AbstractAdmin {



    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            ->add('brand')
            ->add('typeVehicle')
            ->add('model')
            ->add('slug')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('brand')
            ->add('typeVehicle')
            ->add('model')
            ->add('slug')
        ;
    }
    protected function configureShowFields(ShowMapper $showMapper){
        $showMapper
            ->add('id')
            ->add('brand')
            ->add('typeVehicle')
            ->add('model')
            ->add('slug')
        ;
    }

    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->add('id')
            ->add('brand')
            ->add('typeVehicle')
            ->add('model')
            ->add('slug')
            ->add('_action', null, [
                'actions' => [
                    'show'    => [],
                    'edit'    => [],
                    'delete'  => [],
                ]
            ])
        ;
    }
}