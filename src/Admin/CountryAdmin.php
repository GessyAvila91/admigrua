<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: CountryAdminAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/CountryAdmin.phpn.php
 * project: Admigrua2
 * File: CountryAdminAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CountryAdmin extends AbstractAdmin {

	protected function configureFormFields (FormMapper $formMapper) {
		$formMapper
			->add('code')
			->add('name');
	}

	protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
		$datagridMapper

			->add('name');
	}

	protected function configureShowFields (ShowMapper $showMapper) {
		$showMapper

			->add('name');
	}

	protected function configureListFields (ListMapper $listMapper) {
		$listMapper

			->add('name')
            ->add('unicodehex','string', ['template' => 'unicodeFlag.html.twig']);
	}

}