<?php

/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/13
 * File: AssigmentAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/AssigmentAdmin.php
 * project: Admigrua2
 * File: AssigmentAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use App\Entity\Note;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;

class AssigmentAdmin extends AbstractAdmin {

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			//->add('id')
			->add('version')
			->add('dye')
			->add('plate')
			->add('dateCapture',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('dateProsess',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('dateContact',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('dateOver',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('datePay',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('dateLastupdate',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
			->add('report')
			->add('sinester')
			->add('location')
			->add('destination')
			->add('costLeadChangeName')
			->add('distance')
			->add('costDistance')
			->add('guardDay')
			->add('costGuard')
			->add('costExpired')
			->add('extraLoad')
			->add('costLoad')
			->add('handling')
			->add('costHandling')
			->add('armor')
			->add('dolly')
			->add('amount')
			->add('amountaux')
			->add('bill')
			->add('billfilepdf')
			->add('billfilexml')
			->add('taxValue')
			->add('coment')
			->add('assistanceVehicle')
			->add('customer')
			->add('statusAssigment')
			->add('statusPay')
			->add('typeAssigment')
			->add('typeBills')
			->add('userCode')
			->add('vehicle')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('id')
			->add('version')
			->add('dye')
			->add('plate')
			->add('dateCapture')
			->add('dateProsess')
			->add('dateContact')
			->add('dateOver')
			->add('datePay')
			->add('dateLastupdate')
			->add('report')
			->add('sinester')
			->add('location')
			->add('destination')
			->add('costLeadChangeName')
			->add('distance')
			->add('costDistance')
			->add('guardDay')
			->add('costGuard')
			->add('costExpired')
			->add('extraLoad')
			->add('costLoad')
			->add('handling')
			->add('costHandling')
			->add('armor')
			->add('dolly')
			->add('amount')
			->add('amountaux')
			->add('bill')
			->add('billfilepdf')
			->add('billfilexml')
			->add('taxValue')
			->add('coment')
			->add('assistanceVehicle')
			->add('customer')
			->add('statusAssigment')
			->add('statusPay')
			->add('typeAssigment')
			->add('typeBills')
			->add('userCode')
			->add('vehicle')

		;
	}

	public function configureShowFields (ShowMapper $showMapper) {
		$showMapper
			->add('id')
			->add('version')
			->add('dye')
			->add('plate')
			->add('dateCapture')
			->add('dateProsess')
			->add('dateContact')
			->add('dateOver')
			->add('datePay')
			->add('dateLastupdate')
			->add('report')
			->add('sinester')
			->add('location')
			->add('destination')
			->add('costLeadChangeName')
			->add('distance')
			->add('costDistance')
			->add('guardDay')
			->add('costGuard')
			->add('costExpired')
			->add('extraLoad')
			->add('costLoad')
			->add('handling')
			->add('costHandling')
			->add('armor')
			->add('dolly')
			->add('amount')
			->add('amountaux')
			->add('bill')
			->add('billfilepdf')
			->add('billfilexml')
			->add('taxValue')
			->add('coment')
			->add('assistanceVehicle')
			->add('customer')
			->add('statusAssigment')
			->add('statusPay')
			->add('typeAssigment')
			->add('typeBills')
			->add('userCode')
			->add('vehicle')
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
            //->add('notecount')
            //->add('note',null, ['template' => 'serviceNote.html.twig'])
            ->add('statusPay', 'string', ['template' => 'statusPayColor.html.twig'])
            ->add('statusAssigment', 'string', ['template' => 'statusAssigmentColor.html.twig'])

            //->add('statusAssigment')
			//->add('version')
//			->add('dye')
			->add('plate', 'string', ['template' => 'plateCarModel.html.twig'])

            ->add('timediff','number', ['template' => 'timeDiffColor.html.twig'])

			->add('dateCapture','datetime'
                ,['format' => 'Y/M/d H:m'])
            ->add('dateContact','datetime'
                ,['format' => 'Y/M/d H:m'])
			->add('dateProsess','datetime'
                ,['format' => 'Y/M/d H:m'])

//			->add('dateOver')
//			->add('datePay')

//			->add('dateLastupdate')

			->add('report')
			->add('sinester')
			/*->add('location')
			->add('destination')*/
			/*->add('costLeadChangeName')
			->add('distance')
			->add('costDistance')
			->add('guardDay')
			->add('costGuard')
			->add('costExpired')
			->add('extraLoad')
			->add('costLoad')
			->add('handling')
			->add('costHandling')
			->add('armor')
			->add('dolly')
			->add('amount')
			->add('amountaux')
			->add('bill')
			->add('billfilepdf')
			->add('billfilexml')
			->add('taxValue')
			//->add('coment')
			->add('assistanceVehicle')
			->add('customer')
			//->add('statusAssigment')
			//->add('statusPay')
			->add('typeAssigment')
			->add('typeBills')
			->add('userCode')*/
			->add('vehicle')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
		;
	}
}
