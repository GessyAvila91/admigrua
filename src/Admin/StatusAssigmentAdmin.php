<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: StatusAssigmentAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/StatusAssigmentAdmin.php
 * project: Admigrua2
 * File: StatusAssigmentAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StatusAssigmentAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            ->add('id')
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->addIdentifier('id')
            ->add('name', 'string', ['template' => 'statusAssigmentColorName.html.twig'])
            ->add('description')
        ;
    }
}