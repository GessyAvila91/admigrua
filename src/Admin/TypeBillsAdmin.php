<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: TypeBillsAdminn.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/TypeBillsAdmin.phpp
 * project: Admigrua2
 * File: TypeBillsAdminn.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TypeBillsAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            ->add('name'       )
            ->add('description')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description');
    }
    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('description');
    }

    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('description')
        ;
    }

}