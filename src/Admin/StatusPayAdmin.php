<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StatusPayAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('description')
        ;
    }
    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('description')
        ;
    }
}