<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: Maintenance.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/Maintenance.php
 * project: Admigrua2
 * File: Maintenance.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DatePickerType;

class MaintenanceAdmin {
    protected function configureFormFields (FormMapper $formMapper) {
        $formMapper
            ->add('expense')
            ->add('assistanceVehicle')
            ->add('date',DatePickerType::class)
            ->add('description')
            ->add('timeStamp',DatePickerType::class)

        ;
    }

    protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }

    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }

    protected function configureListFields (ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }
}