<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: NoteAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/NoteAdmin.php
 * project: Admigrua2
 * File: NoteAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;

class NoteAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('id')
            ->add('date',DateTimePickerType::class,[
                'dp_side_by_side'       => true,
                'dp_use_current'        => true,
                'dp_use_seconds'        => true,
                'dp_collapse'           => false,
                'dp_calendar_weeks'     => true,
                'dp_view_mode'          => 'days',
                'dp_min_view_mode'      => 'days',
            ])
            ->add('conten')
            //->add('assigment')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('date')
            ->add('conten')
//            ->add('assigment')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('date')
            ->add('conten')
//            ->add('assigment')
        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id')
            ->add('date','datetime'
                ,['format' => 'Y/M/d H:m'])
            ->add('conten')
//            ->add('assigment')
        ;
    }
}