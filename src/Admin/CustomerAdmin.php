<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: CustomerAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/CustomerAdmin.php
 * project: Admigrua2
 * File: CustomerAdmin.php
 * *********************************************************************
 */


/**
 * ->add('id')
 *  ->add('name')
 * ->add('email')
 * ->add('aLead')
 * ->add('aExpired')
 * ->add('aGuard')
 * ->add('bLead')
 * ->add('bExpired')
 * ->add('bDistance')
 * ->add('bGuard')
 * ->add('dolly')
 * ->add('costLoad')
 * ->add('armorA')
 * ->add('armorB')
 * ->add('basementCost')
 * ->add('creationDate')
 * ->add('updateDate')
 * ->add('active')
 **/

namespace App\Admin;

use phpDocumentor\Reflection\Types\Boolean;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Type\DateTimePickerType;


class CustomerAdmin extends AbstractAdmin {

    protected function configureFormFields (FormMapper $formMapper) {
        $formMapper
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('aLead')
            ->add('aExpired')
            ->add('aGuard')
            ->add('bLead')
            ->add('bExpired')
            ->add('bDistance')
            ->add('bGuard')
            ->add('dolly')
            ->add('costLoad')
            ->add('armorA')
            ->add('armorB')
            ->add('basementCost')
            ->add('creationDate', DateTimePickerType::class)
            ->add('updateDate', DateTimePickerType::class)
            ->add('active')
        ;
    }

    protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('aLead')
            ->add('aExpired')
            ->add('aGuard')
            ->add('bLead')
            ->add('bExpired')
            ->add('bDistance')
            ->add('bGuard')
            ->add('dolly')
            ->add('costLoad')
            ->add('armorA')
            ->add('armorB')
            ->add('basementCost')
            ->add('creationDate')
            ->add('updateDate')
            ->add('active');
    }

    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('aLead')
            ->add('aExpired')
            ->add('aGuard')
            ->add('bLead')
            ->add('bExpired')
            ->add('bDistance')
            ->add('bGuard')
            ->add('dolly')
            ->add('costLoad')
            ->add('armorA')
            ->add('armorB')
            ->add('basementCost')
            ->add('creationDate')
            ->add('updateDate')
            ->add('active');
    }

    protected function configureListFields (ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('email')
            ->add('aLead')
            ->add('aExpired')
            ->add('aGuard')
            ->add('bLead')
            ->add('bExpired')
            ->add('bDistance')
            ->add('bGuard')
            ->add('dolly')
            ->add('costLoad')
            ->add('armorA')
            ->add('armorB')
            ->add('basementCost')
            ->add('creationDate')
            ->add('updateDate')
            ->add('active');
    }
}