<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: TypeAssistanceVehicleAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/TypeAssistanceVehicleAdmin.php
 * project: Admigrua2
 * File: TypeAssistanceVehicleAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TypeAssistanceVehicleAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper){
        $formMapper
            ->add('name'       , TextType::class)
            ->add('description', TextType::class)
            ->add('icon'       , TextType::class)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('icon')
        ;
    }
    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('icon','string', ['template' => 'FaFaIcon.html.twig']);
    }

    protected function configureListFields(ListMapper $listMapper){
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('description')
            ->add('icon','string', ['template' => 'FaFaIcon.html.twig']);
        ;
    }

}