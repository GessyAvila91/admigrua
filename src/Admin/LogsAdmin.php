<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: LogsAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/LogsAdmin.php
 * project: Admigrua2
 * File: LogsAdmin.php
 * *********************************************************************
 */

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DatePickerType;

class LogsAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('id')
            ->add('automata')
            ->add('comments')
            ->add('date',DatePickerType::class)
            ->add('data')
            ->add('userCode')
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('automata')
            ->add('comments')
            ->add('date')
            ->add('userCode')
        ;
    }
    public function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('id')
            ->add('automata')
            ->add('comments')
            ->add('date')
            ->add('userCode')
        ;
    }
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id')
            ->add('automata')
            ->add('comments')
            ->add('date')
            ->add('userCode')
        ;
    }

}