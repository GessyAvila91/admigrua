<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/12
 * File: GasAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/GasAdmin.php
 * project: Admigrua2
 * File: GasAdmin.php
 * *********************************************************************
 */

/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/11
 * File: CustomerAdmin.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Admin/CustomerAdmin.php
 * project: Admigrua2
 * File: CustomerAdmin.php
 * *********************************************************************
 */

namespace App\Admin;

use phpDocumentor\Reflection\Types\Boolean;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Type\DateTimePickerType;


class GasAdmin extends AbstractAdmin {

    protected function configureFormFields (FormMapper $formMapper) {
        $formMapper
            ->add('expense')
            ->add('km')
            ->add('litros')
            //->add('timeStamp',DatePickerType::class)
            ->add('assistanceVehicle')
        ;
    }

    protected function configureDatagridFilters (DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }

    protected function configureShowFields (ShowMapper $showMapper) {
        $showMapper
            ->add('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }

    protected function configureListFields (ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('expense')
            ->add('km')
            ->add('litros')
            ->add('timeStamp')
            ->add('assistanceVehicle');
    }
}