<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/25
 * File: StatsCRUDController.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Controller/StatsCRUDController.php
 * project: Admigrua2
 * File: StatsCRUDController.php
 * *********************************************************************
 */

namespace App\Controller;


use Sonata\AdminBundle\Controller\CRUDController;

class StatsCRUDController extends CRUDController {
    public function listAction() {
        return $this->renderWithExtraParams('stats.html.twig');
    }
}