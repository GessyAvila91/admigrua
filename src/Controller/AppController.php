<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/26
 * File: AppController.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Controller/AppController.php
 * project: Admigrua2
 * File: AppController.php
 * *********************************************************************
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mukadi\Chart\Utils\RandomColorFactory;
use Mukadi\Chart\Chart;

class AppController extends Controller{

    public function chart() {
        $builder = $this->get('mukadi_chart_js.dql');
        $builder
            ->query("SELECT COUNT(*) FROM App:Assigment ")
            ->addDataset('total','Total',[
                "backgroundColor" => RandomColorFactory::getRandomRGBAColors(6)
            ])
            ->labels('type')
        ;
        $chart = $builder->buildChart('my_chart',Chart::PIE);
        return $this->render('chart.html.twig',[
            "chart" => $chart,
        ]);
    }
}