<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/7/25
 * File: StatsBlockService.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Block/Service/StatsBlockService.php
 * project: Admigrua2
 * File: StatsBlockService.php
 * *********************************************************************
 */

namespace App\Block\Service;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Form\FormMapper;
//use Sonata\BlockBundle\Templating\EngineInterface;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bundle\TwigBundle\TwigEngine;
//use Twig\Environment;
use Symfony\Component\Templating\EngineInterface;

class StatsBlockService extends AbstractBlockService  {

    private $entityManager;


    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'Stats Block';
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)  {
        $resolver->setDefaults(array(
            'entity' => 'App:Brand',
            'repository_method' => 'findAll',
            'title' => 'Insert block Title',
            'css_class' => 'bg-blue',
            'icon' => 'fa-users',
            'template' => 'block_stats.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block){
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('entity', 'text', array('required' => false)),
                array('repository_method', 'text', array('required' => false)),
                array('title', 'text', array('required' => false)),
                array('css_class', 'text', array('required' => false)),
                array('icon', 'text', array('required' => false)),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {
        $errorElement
            ->with('settings[entity]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[repository_method]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertMaxLength(array('limit' => 50))
            ->end()
            ->with('settings[css_class]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[icon]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null) {
        $settings = $blockContext->getSettings();
        $entity = $settings['entity'];
        $method = $settings['repository_method'];

        //$rows = $this->entityManager->getRepository('App:Brand')->$method();

        return $this->templating->renderResponse(
            $blockContext->getTemplate(), array(
            'count'     => '1',
            'block'     => 'block_stats.html.twig',
            'settings'  => $settings,
        ), $response);
    }
}

//https://kunicmarko20.github.io/2017/09/17/Sonata-Admin-Custom-Page.html