<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gas
 *
 * @ORM\Table(name="gas", indexes={@ORM\Index(name="fk_gas_expense1_idx", columns={"expense_id"}), @ORM\Index(name="fk_gas_assistance_vehicle_idx", columns={"assistance_vehicle_id"})})
 * @ORM\Entity
 */
class Gas  {
    /**
     * @var string|null
     *
     * @ORM\Column(name="km", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $km;

    /**
     * @var string|null
     *
     * @ORM\Column(name="litros", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $litros;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=true)
     */
    private $timeStamp;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \Expense
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expense")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expense_id", referencedColumnName="id")
     * })
     */
    private $expense;

    /**
     * @return string|null
     */
    public function getKm (){
        return $this->km;
    }

    /**
     * @param string|null $km
     */
    public function setKm ($km){
        $this->km = $km;
    }

    /**
     * @return string|null
     */
    public function getLitros (){
        return $this->litros;
    }

    /**
     * @param string|null $litros
     */
    public function setLitros ($litros){
        $this->litros = $litros;
    }

    /**
     * @return \DateTime|null
     */
    public function getTimeStamp (){
        return $this->timeStamp;
    }

    /**
     * @param \DateTime|null $timeStamp
     */
    public function setTimeStamp ($timeStamp){
        $this->timeStamp = $timeStamp;
    }

    /**
     * @return \AssistanceVehicle
     */
    public function getAssistanceVehicle () {
        return $this->assistanceVehicle;
    }

    /**
     * @param \AssistanceVehicle $assistanceVehicle
     */
    public function setAssistanceVehicle ($assistanceVehicle){
        $this->assistanceVehicle = $assistanceVehicle;
    }

    /**
     * @return \Expense
     */
    public function getExpense (){
        return $this->expense;
    }

    /**
     * @param \Expense $expense
     */
    public function setExpense ($expense){
        $this->expense = $expense;
    }



}
