<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment"="Nombre o razon social del cliente"})
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true, options={"comment"="correo del usuario"})
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="a_lead", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de banderaso"})
     */
    private $aLead;

    /**
     * @var string|null
     *
     * @ORM\Column(name="a_expired", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de servicio muerto"})
     */
    private $aExpired;

    /**
     * @var string|null
     *
     * @ORM\Column(name="a_distance", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de km"})
     */
    private $aDistance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="a_guard", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de resguardo"})
     */
    private $aGuard;

    /**
     * @var string|null
     *
     * @ORM\Column(name="b_lead", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de banderaso"})
     */
    private $bLead;

    /**
     * @var string|null
     *
     * @ORM\Column(name="b_expired", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de servicio muerto"})
     */
    private $bExpired;

    /**
     * @var string|null
     *
     * @ORM\Column(name="b_distance", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de km"})
     */
    private $bDistance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="b_guard", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo de resguardo"})
     */
    private $bGuard;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dolly", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo por uso del dolly"})
     */
    private $dolly;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_load", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo por peso extra (kg)"})
     */
    private $costLoad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="armor_a", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo traslado de vehiculo blindado A"})
     */
    private $armorA;

    /**
     * @var string|null
     *
     * @ORM\Column(name="armor_b", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo traslado de vehiculo blindado B"})
     */
    private $armorB;

    /**
     * @var string|null
     *
     * @ORM\Column(name="basement_cost", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo por nivel de sotano"})
     */
    private $basementCost;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true, options={"default"="2019-07-03 00:00:00"})
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $active;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName( $name){
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getEmail(){
		return $this->email;
	}

	/**
	 * @param string|null $email
	 */
	public function setEmail( $email){
		$this->email = $email;
	}

	/**
	 * @return string|null
	 */
	public function getALead(){
		return $this->aLead;
	}

	/**
	 * @param string|null $aLead
	 */
	public function setALead( $aLead){
		$this->aLead = $aLead;
	}

	/**
	 * @return string|null
	 */
	public function getAExpired(){
		return $this->aExpired;
	}

	/**
	 * @param string|null $aExpired
	 */
	public function setAExpired( $aExpired){
		$this->aExpired = $aExpired;
	}

	/**
	 * @return string|null
	 */
	public function getADistance(){
		return $this->aDistance;
	}

	/**
	 * @param string|null $aDistance
	 */
	public function setADistance( $aDistance){
		$this->aDistance = $aDistance;
	}

	/**
	 * @return string|null
	 */
	public function getAGuard(){
		return $this->aGuard;
	}

	/**
	 * @param string|null $aGuard
	 */
	public function setAGuard($aGuard){
		$this->aGuard = $aGuard;
	}

	/**
	 * @return string|null
	 */
	public function getBLead(){
		return $this->bLead;
	}

	/**
	 * @param string|null $bLead
	 */
	public function setBLead($bLead){
		$this->bLead = $bLead;
	}

	/**
	 * @return string|null
	 */
	public function getBExpired(){
		return $this->bExpired;
	}

	/**
	 * @param string|null $bExpired
	 */
	public function setBExpired($bExpired){
		$this->bExpired = $bExpired;
	}

	/**
	 * @return string|null
	 */
	public function getBDistance(){
		return $this->bDistance;
	}

	/**
	 * @param string|null $bDistance
	 */
	public function setBDistance($bDistance){
		$this->bDistance = $bDistance;
	}

	/**
	 * @return string|null
	 */
	public function getBGuard(){
		return $this->bGuard;
	}

	/**
	 * @param string|null $bGuard
	 */
	public function setBGuard($bGuard){
		$this->bGuard = $bGuard;
	}

	/**
	 * @return string|null
	 */
	public function getDolly(){
		return $this->dolly;
	}

	/**
	 * @param string|null $dolly
	 */
	public function setDolly($dolly){
		$this->dolly = $dolly;
	}

	/**
	 * @return string|null
	 */
	public function getCostLoad(){
		return $this->costLoad;
	}

	/**
	 * @param string|null $costLoad
	 */
	public function setCostLoad($costLoad){
		$this->costLoad = $costLoad;
	}

	/**
	 * @return string|null
	 */
	public function getArmorA(){
		return $this->armorA;
	}

	/**
	 * @param string|null $armorA
	 */
	public function setArmorA($armorA){
		$this->armorA = $armorA;
	}

	/**
	 * @return string|null
	 */
	public function getArmorB(){
		return $this->armorB;
	}

	/**
	 * @param string|null $armorB
	 */
	public function setArmorB($armorB){
		$this->armorB = $armorB;
	}

	/**
	 * @return string|null
	 */
	public function getBasementCost(){
		return $this->basementCost;
	}

	/**
	 * @param string|null $basementCost
	 */
	public function setBasementCost($basementCost){
		$this->basementCost = $basementCost;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreationDate(){
		return $this->creationDate;
	}

	/**
	 * @param \DateTime|null $creationDate
	 */
	public function setCreationDate($creationDate){
		$this->creationDate = $creationDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdateDate(){
		return $this->updateDate;
	}

	/**
	 * @param \DateTime|null $updateDate
	 */
	public function setUpdateDate($updateDate){
		$this->updateDate = $updateDate;
	}

	/**
	 * @return bool|null
	 */
	public function getActive(){
		return $this->active;
	}

	/**
	 * @param bool|null $active
	 */
	public function setActive($active){
		$this->active = $active;
	}

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}

}
