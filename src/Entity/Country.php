<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country {
    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=2, nullable=false, options={"fixed"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $code;

    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @var string|null
     * @ORM\Column(name="unicodehex", type="string", length=255, nullable=true)
     */
    private $unicodehex;
    /**
     * @var string|null
     * @ORM\Column(name="unicodedec", type="string", length=255, nullable=true)
     */
    private $unicodedec;
	/**
	 * @return string
	 */
	public function getCode(){
		return $this->code;
	}

	/**
	 * @param string $code
	 */
	public function setCode($code){
		$this->code = $code;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

    /**
     * @return string|null
     */
    public function getUnicodehex (){
        return $this->unicodehex;
    }

    /**
     * @param string|null $unicode
     */
    public function setUnicodehex ($unicode){
        $this->unicode = $unicode;
    }

    /**
     * @return string|null
     */
    public function getUnicodedec (){
        return $this->unicodedec;
    }

    /**
     * @param string|null $unicodedec
     */
    public function setUnicodedec ($unicodedec){
        $this->unicodedec = $unicodedec;
    }


	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getname();
	}
}
