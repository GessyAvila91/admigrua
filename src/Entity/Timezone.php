<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/15
 * File: Timezone.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Timezone.php
 * project: Admigrua2
 * File: Timezone.php
 * *********************************************************************
 */

/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/15
 * File: Timezone.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Old/Timezone.php
 * project: Admigrua2
 * File: Timezone.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timezone
 *
 * @ORM\Table(name="timezone", indexes={@ORM\Index(name="fk_timezone_zone1_idx", columns={"zone_id"})})
 * @ORM\Entity
 */
class Timezone
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
    /**
     * @var string|null
     *
     * @ORM\Column(name="abbreviartion", type="string", length=6, nullable=true)
     */
    private $abbreviartion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="time_start", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $timeStart;

    /**
     * @var int|null
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
	 * @var bool|null
     *
     * @ORM\Column(name="dst", type="boolean", nullable=true, options={"default"="1"})
     */
    private $dst;






    /**
     * @var \Zone
     * @ORM\OneToOne(targetEntity="Zone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zone_id", referencedColumnName="id")
     * })
     */
    private $zone;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getAbbreviartion(){
		return $this->abbreviartion;
	}

	/**
	 * @param string|null $abbreviartion
	 */
	public function setAbbreviartion($abbreviartion){
		$this->abbreviartion = $abbreviartion;
	}

	/**
	 * @return string|null
	 */
	public function getTimeStart(){
		return $this->timeStart;
	}

	/**
	 * @param string|null $timeStart
	 */
	public function setTimeStart($timeStart){
		$this->timeStart = $timeStart;
	}

	/**
	 * @return int|null
	 */
	public function getGmtOffset(){
		return $this->gmtOffset;
	}

	/**
	 * @param int|null $gmtOffset
	 */
	public function setGmtOffset($gmtOffset){
		$this->gmtOffset = $gmtOffset;
	}

	/**
	 * @return bool|null
	 */
	public function getDst(){
		return $this->dst;
	}

	/**
	 * @param bool|null $dst
	 */
	public function setDst($dst){
		$this->dst = $dst;
	}

	/**
	 * @return \Zone
	 */
	public function getZone(){
		return $this->zone;
	}

	/**
	 * @param \Zone $zone
	 */
	public function setZone($zone){
		$this->zone = $zone;
	}

    public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getAbbreviartion();
	}

}
