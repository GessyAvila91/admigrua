<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="logs", indexes={@ORM\Index(name="fk_logs_user1_idx", columns={"user_code"})})
 * @ORM\Entity
 */
class Logs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="automata", type="string", length=50, nullable=true)
     */
    private $automata;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true, options={"comment"="Comentarios para el desarrollador"})
     */
    private $comments;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true, options={"comment"="fecha de captura"})
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data", type="string", length=1023, nullable=true, options={"comment"="metadata"})
     */
    private $data;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_code", referencedColumnName="code")
     * })
     */
    private $userCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getAutomata(){
		return $this->automata;
	}

	/**
	 * @param string|null $automata
	 */
	public function setAutomata($automata){
		$this->automata = $automata;
	}

	/**
	 * @return string|null
	 */
	public function getComments() {
		return $this->comments;
	}

	/**
	 * @param string|null $comments
	 */
	public function setComments($comments){
		$this->comments = $comments;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDate(){
		return $this->date;
	}

	/**
	 * @param \DateTime|null $date
	 */
	public function setDate($date){
		$this->date = $date;
	}

	/**
	 * @return string|null
	 */
	public function getData(){
		return $this->data;
	}

	/**
	 * @param string|null $data
	 */
	public function setData($data){
		$this->data = $data;
	}

	/**
	 * @return \User
	 */
	public function getUserCode(){
		return $this->userCode;
	}

	/**
	 * @param \User $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}


}
