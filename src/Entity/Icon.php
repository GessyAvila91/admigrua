<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/16
 * File: Icon.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Icon.php
 * project: Admigrua2
 * File: Icon.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IconAdmin
 *
 * @ORM\Table(name="icon")
 * @ORM\Entity
 */
class Icon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;
    /**
     * @var string|null
     * @ORM\Column(name="codehex", type="string", length=255, nullable=true)
     */
    private $codehex;
    /**
     * @var string|null
     * @ORM\Column(name="fontawesome", type="string", length=255, nullable=true)
     */
    private $fontawesome;

    /**
     * @return string|null
     */
    public function getCodehex (){
        return $this->codehex;
    }

    /**
     * @param string|null $codehex
     */
    public function setCodehex ($codehex){
        $this->codehex = $codehex;
    }

    /**
     * @return string|null
     */
    public function getFontawesome (){
        return $this->fontawesome;
    }

    /**
     * @param string|null $fontawesome
     */
    public function setFontawesome ($fontawesome){
        $this->fontawesome = $fontawesome;
    }


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getFilename(){
		return $this->filename;
	}

	/**
	 * @param string|null $filename
	 */
	public function setFilename($filename){
		$this->filename = $filename;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreatedDate(){
		return $this->createdDate;
	}

	/**
	 * @param \DateTime|null $createdDate
	 */
	public function setCreatedDate($createdDate){
		$this->createdDate = $createdDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdatedDate(){
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime|null $updatedDate
	 */
	public function setUpdatedDate($updatedDate){
		$this->updatedDate = $updatedDate;
	}


    public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}
}
