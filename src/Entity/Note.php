<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="note", indexes={@ORM\Index(name="fk_note_assigment1_idx", columns={"assigment_id"})})
 * @ORM\Entity
 */
class Note {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true, options={"comment"="Fecha de la nota al ser dada de alta"})
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conten", type="string", length=255, nullable=true, options={"comment"="Contenido de la nota"})
     */
    private $conten;

    //todo para notas en asistencias https://github.com/sonata-project/SonataAdminBundle/issues/2890
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Assigment")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="assigment_id", referencedColumnName="id")})
     */
    private $assigment;

    /**
     * Note constructor.
     * @param int                                          $id
     * @param \DateTime|null                               $date
     * @param string|null                                  $conten
     * @param \Doctrine\Common\Collections\ArrayCollection $assigment
     */
    public function __construct () {
        $this->date = new \DateTime;
        $this->conten = new ArrayCollection();
    }

    /**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDate(){
		return $this->date;
	}

	/**
	 * @param \DateTime|null $date
	 */
	public function setDate($date){
		$this->date = $date;
	}

	/**
	 * @return string|null
	 */
	public function getConten(){
		return $this->conten;
	}

	/**
	 * @param string|null $conten
	 */
	public function setConten($conten){
		$this->conten = $conten;
	}

	/**
	 * @return \Assigment
	 */
	public function getAssigment(){
		return $this->assigment;
	}

	/**
	 * @param \Assigment $assigment
	 */
	public function setAssigment($assigment){
		$this->assigment = $assigment;
	}
    
    public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getConten();
	}


}
