<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeAssistanceVehicle
 * @ORM\Table(name="type_assistance_vehicle")
 * @ORM\Entity
 */

class TypeAssistanceVehicle  {

    /**
     * @var int
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment"="nombre asignado a el vehiculo"})
     */
    private $name;
    /**
     * @var string|null
     * @ORM\Column(name="description", type="string", length=255, nullable=true, options={"comment"="descripcion del mismo"})
     */
    private $description;
    /**
     * @var string|null
     * @ORM\Column(name="icon", type="string", length=255, nullable=true, options={"comment"="icono del typo"})
     */
    private $icon;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription($description){
		$this->description = $description;
	}

    /**
     * @return string|null
     */
    public function getIcon (){
        return $this->icon;
    }

    /**
     * @param string|null $icon
     */
    public function setIcon ($icon){
        $this->icon = $icon;
    }

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}
}
