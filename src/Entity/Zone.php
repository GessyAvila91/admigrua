<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zone
 *
 * @ORM\Table(name="zone", indexes={@ORM\Index(name="fk_zone_country1_idx", columns={"country_code"})})
 * @ORM\Entity
 */
class Zone
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", length=35, nullable=true)
     */
    private $name;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_code", referencedColumnName="code")
     * })
     */
    private $countryCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return \Country
	 */
	public function getCountryCode(){
		return $this->countryCode;
	}

	/**
	 * @param \Country $countryCode
	 */
	public function setCountryCode($countryCode){
		$this->countryCode = $countryCode;
	}

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}


}
