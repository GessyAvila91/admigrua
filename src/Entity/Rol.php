<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/16
 * File: Rol.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Rol.php
 * project: Admigrua2
 * File: Rol.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rol
 *
 * @ORM\Table(name="rol", indexes={@ORM\Index(name="fk_rol_icon1_idx", columns={"icon_id"})})
 * @ORM\Entity
 */
class Rol
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false, options={"fixed"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="acces", type="string", length=255, nullable=true)
     */
    private $acces;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

//    /**
//     * @var \Icon
//     *
//     * @ORM\ManyToOne(targetEntity="Icon")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="icon_id", referencedColumnName="id")
//     * })
//     */
//    private $icon;

	/**
	 * @return string
	 */
	public function getCode(){
		return $this->code;
	}

	/**
	 * @param string $code
	 */
	public function setCode($code){
		$this->code = $code;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription($description){
		$this->description = $description;
	}

	/**
	 * @return string|null
	 */
	public function getAcces(){
		return $this->acces;
	}

	/**
	 * @param string|null $acces
	 */
	public function setAcces($acces){
		$this->acces = $acces;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreationDate(){
		return $this->creationDate;
	}

	/**
	 * @param \DateTime|null $creationDate
	 */
	public function setCreationDate($creationDate){
		$this->creationDate = $creationDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdatedDate(){
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime|null $updatedDate
	 */
	public function setUpdatedDate($updatedDate){
		$this->updatedDate = $updatedDate;
	}

//	/**
//	 * @return \Icon
//	 */
//	public function getIcon(){
//		return $this->icon;
//	}
//
//	/**
//	 * @param \Icon $icon
//	 */
//	public function setIcon($icon){
//		$this->icon = $icon;
//	}

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}

}
