<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/15
 * File: LogVehicleAssistance.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/LogVehicleAssistance.php
 * project: Admigrua2
 * File: LogVehicleAssistance.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogVehicleAssistance
 *
 * @ORM\Table(name="log_vehicle_assistance", indexes={@ORM\Index(name="fk_log_vehicle_assistance_assistance_vehicle1_idx", columns={"assistance_vehicle_id"}), @ORM\Index(name="fk_log_vehicle_assistance_user1_idx", columns={"user_code"})})
 * @ORM\Entity
 */
class LogVehicleAssistance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateIN", type="datetime", nullable=true)
     */
    private $datein;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateOUT", type="datetime", nullable=true)
     */
    private $dateout;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_code", referencedColumnName="code")
     * })
     */
    private $userCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDatein(){
		return $this->datein;
	}

	/**
	 * @param \DateTime|null $datein
	 */
	public function setDatein($datein){
		$this->datein = $datein;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDateout(){
		return $this->dateout;
	}

	/**
	 * @param \DateTime|null $dateout
	 */
	public function setDateout($dateout){
		$this->dateout = $dateout;
	}

	/**
	 * @return \AssistanceVehicle
	 */
	public function getAssistanceVehicle(){
		return $this->assistanceVehicle;
	}

	/**
	 * @param \AssistanceVehicle $assistanceVehicle
	 */
	public function setAssistanceVehicle($assistanceVehicle){
		$this->assistanceVehicle = $assistanceVehicle;
	}

	/**
	 * @return \User
	 */
	public function getUserCode(){
		return $this->userCode;
	}

	/**
	 * @param \User $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}


    public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getId()."";
	}

}
