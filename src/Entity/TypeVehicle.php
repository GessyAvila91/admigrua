<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeVehicle
 *
 * @ORM\Table(name="type_vehicle")
 * @ORM\Entity
 */
class TypeVehicle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment"="Nombre del tipo de vehiculo Ejm {A,B,C,D}"})
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(?string $description) {
		$this->description = $description;
	}


	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}

}
