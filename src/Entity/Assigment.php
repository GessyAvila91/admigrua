<?php
/**
 * *********************************************************************
 * Usuario: geavila
 * Date: 2019/9/24
 * File: Assigment.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Assigment.php
 * project: Admigrua2
 * File: Assigment.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Note;
use App\Entity\StatusPay;

/**
 * Assigment
 *
 * @ORM\Table(name="assigment", indexes={@ORM\Index(name="fk_assigment_user1_idx", columns={"user_code"}), @ORM\Index(name="fk_assigment_assistance_vehicle1_idx", columns={"assistance_vehicle_id"}), @ORM\Index(name="fk_assigment_status_pay1_idx", columns={"status_pay_id"}), @ORM\Index(name="fk_assigment_customer1_idx", columns={"customer_id"}), @ORM\Index(name="fk_assigment_type_bills1_idx", columns={"type_bills_id"}), @ORM\Index(name="fk_assigment_vehicle1_idx", columns={"vehicle_id"}), @ORM\Index(name="fk_assigment_status_assigment1_idx", columns={"status_assigment_id"}), @ORM\Index(name="fk_assigment_type_assigment1_idx", columns={"type_assigment_id"})})
 * @ORM\Entity
 */
class Assigment  {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

//todo para notas en asistencias https://github.com/sonata-project/SonataAdminBundle/issues/2890
//    /**
//     * @var ArrayCollection
//     *
//     * @ORM\OneToMany(targetEntity="Note", inversedBy="Assigment")
//     */
//    private $note;

    /**
     * @return ArrayCollection
     */
    public function getNote (){
        return $this->note;
    }

    public function getNotecount (){
        //echo $this->getNote()->count();
        return 5;
    }

    /**
     * @var string|null
     * @ORM\Column(name="version", type="string", length=50, nullable=true, options={"comment"="año y version del vehiculo"})
     */
    private $version;

    /**
     * @var string|null
     * @ORM\Column(name="dye", type="string", length=20, nullable=true, options={"comment"="color del vehiculo"})
     */
    private $dye;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plate", type="string", length=10, nullable=true, options={"comment"="placas del vehiculo"})
     */
    private $plate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_capture", type="datetime", nullable=true, options={"comment"="fecha de captura"})
     */
    private $dateCapture;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_prosess", type="datetime", nullable=true, options={"comment"="time stamp de cuando entra a proceso"})
     */
    private $dateProsess;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_contact", type="datetime", nullable=true, options={"comment"="time stamp de cuando entra en contacto"})
     */
    private $dateContact;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_over", type="datetime", nullable=true, options={"comment"="time stamp de cuando se termina"})
     */
    private $dateOver;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_pay", type="datetime", nullable=true, options={"comment"="time stamp del pago del servicio"})
     */
    private $datePay;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_lastupdate", type="datetime", nullable=true, options={"comment"="las update del servicio"})
     */
    private $dateLastupdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report", type="string", length=50, nullable=true, options={"comment"="Numero de reporte"})
     */
    private $report;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sinester", type="string", length=50, nullable=true, options={"comment"="Numero del siniestro"})
     */
    private $sinester;

    /**
     * @var string|null
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true, options={"comment"="ubicacion del vehiculo a asistir"})
     */
    private $location;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destination", type="string", length=255, nullable=true, options={"comment"="destino del vehiculo"})
     */
    private $destination;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_lead_change_name", type="decimal", precision=12, scale=2, nullable=true, options={"comment"="costo del banderazo"})
     */
    private $costLeadChangeName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="distance", type="integer", nullable=true, options={"comment"="km recorridos"})
     */
    private $distance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_distance", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="costo por distancia del servicio"})
     */
    private $costDistance;

    /**
     * @var int|null
     *
     * @ORM\Column(name="guard_day", type="integer", nullable=true, options={"comment"="dias de resguardo del vehiculo"})
     */
    private $guardDay;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_guard", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $costGuard;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_expired", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $costExpired;

    /**
     * @var int|null
     *
     * @ORM\Column(name="extra_load", type="integer", nullable=true, options={"comment"="cantidad de carga extra en Kg"})
     */
    private $extraLoad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_load", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="costo de la carga extra del servicio"})
     */
    private $costLoad;

    /**
     * @var int|null
     *
     * @ORM\Column(name="handling", type="integer", nullable=true, options={"comment"="cantidad de maniobras del vehiculo"})
     */
    private $handling;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cost_handling", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="costo de la maniobra / es equibalente a el banderazo o Lead del servicio"})
     */
    private $costHandling;

    /**
     * @var string|null
     *
     * @ORM\Column(name="armor", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="costo de transporte de vehiculo blindado"})
     */
    private $armor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dolly", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="costo del dolly"})
     */
    private $dolly;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="importe"})
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amountAux", type="decimal", precision=12, scale=2, nullable=true, options={"default"="0.00","comment"="importe"})
     */
    private $amountaux;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bill", type="string", length=50, nullable=true, options={"default"="F/P","comment"="numero de nota o factura correspondiente a el servicio"})
     */
    private $bill = 'F/P';

    /**
     * @var string|null
     *
     * @ORM\Column(name="billFilePDF", type="string", length=100, nullable=true, options={"comment"="Archivo PDF de la factura"})
     */
    private $billfilepdf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="billFileXml", type="string", length=100, nullable=true, options={"comment"="Archivo XML de la Factura"})
     */
    private $billfilexml;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tax_value", type="decimal", precision=5, scale=2, nullable=true, options={"default"="0.16"})
     */
    private $taxValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coment", type="string", length=255, nullable=true, options={"comment"="comentarios anexos del servicio"})
     */
    private $coment;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \StatusAssigment
     *
     * @ORM\ManyToOne(targetEntity="StatusAssigment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_assigment_id", referencedColumnName="id")
     * })
     */
    private $statusAssigment;

    /**
     * @var \StatusPay
     * @ORM\ManyToOne(targetEntity="StatusPay")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="status_pay_id", referencedColumnName="id")})
     */
    private $statusPay;

    /**
     * @var \TypeAssigment
     *
     * @ORM\ManyToOne(targetEntity="TypeAssigment")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="type_assigment_id", referencedColumnName="id")})
     */
    private $typeAssigment;

    /**
     * @var \TypeBills
     *
     * @ORM\ManyToOne(targetEntity="TypeBills")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="type_bills_id", referencedColumnName="id")})
     */
    private $typeBills;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="user_code", referencedColumnName="code")})
     */
    private $userCode;

    /**
     * @var \Vehicle
     *
     * @ORM\ManyToOne(targetEntity="Vehicle")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")})
     */
    private $vehicle;

    /**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}/**
	 * @return \AssistanceVehicle
	 */
	public function getAssistanceVehicle(){
		return $this->assistanceVehicle;
	}/**
	 * @param \AssistanceVehicle $assistanceVehicle
	 */
	public function setAssistanceVehicle($assistanceVehicle){
		$this->assistanceVehicle = $assistanceVehicle;
	}/**
	 * @return \Customer
	 */
	public function getCustomer(){
		return $this->customer;
	}/**
	 * @param \Customer $customer
	 */
	public function setCustomer($customer){
		$this->customer = $customer;
	}/**
	 * @return \StatusAssigment
	 */
	public function getStatusAssigment(){
		return $this->statusAssigment;
	}/**
	 * @param \StatusAssigment $statusAssigment
	 */
	public function setStatusAssigment($statusAssigment){
		$this->statusAssigment = $statusAssigment;
	}/**
	 * @return \StatusPay
	 */
	public function getStatusPay(){
		return $this->statusPay;
	}/**
	 * @param \StatusPay $statusPay
	 */
	public function setStatusPay($statusPay){
		$this->statusPay = $statusPay;
	}/**
	 * @return \TypeAssigment
	 */
	public function getTypeAssigment(){
		return $this->typeAssigment;
	}/**
	 * @param \TypeAssigment $typeAssigment
	 */
	public function setTypeAssigment($typeAssigment){
		$this->typeAssigment = $typeAssigment;
	}/**
	 * @return \TypeBills
	 */
	public function getTypeBills(){
		return $this->typeBills;
	}/**
	 * @param \TypeBills $typeBills
	 */
	public function setTypeBills($typeBills){
		$this->typeBills = $typeBills;
	}/**
	 * @return \User
	 */
	public function getUserCode(){
		return $this->userCode;
	}/**
	 * @param \User $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}/**
	 * @return \Vehicle
	 */
	public function getVehicle(){
		return $this->vehicle;
	}/**
	 * @param \Vehicle $vehicle
	 */
	public function setVehicle($vehicle){
		$this->vehicle = $vehicle;
	}

    /**
     * @return string|null
     */
    public function getVersion (){
        return $this->version;
    }

    /**
     * @param string|null $version
     */
    public function setVersion ( $version){
        $this->version = $version;
    }

    /**
     * @return string|null
     */
    public function getDye (){
        return $this->dye;
    }

    /**
     * @param string|null $dye
     */
    public function setDye ( $dye){
        $this->dye = $dye;
    }

    /**
     * @return string|null
     */
    public function getPlate (){
        return $this->plate;
    }

    /**
     * @param string|null $plate
     */
    public function setPlate ( $plate){
        $this->plate = $plate;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCapture (){
        return $this->dateCapture;
    }

    /**
     * @param \DateTime|null $dateCapture
     */
    public function setDateCapture ($dateCapture){
        //var_dump($dateCapture);
        //die();
        $this->dateCapture = $dateCapture;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateProsess (){
        return $this->dateProsess;
    }

    /**
     * @param \DateTime|null $dateProsess
     */
    public function setDateProsess ( $dateProsess){
        $this->dateProsess = $dateProsess;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateContact (){
        return $this->dateContact;
    }

    /**
     * @param \DateTime|null $dateContact
     */
    public function setDateContact ( $dateContact){
        $this->dateContact = $dateContact;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateOver (){
        return $this->dateOver;
    }

    /**
     * @param \DateTime|null $dateOver
     */
    public function setDateOver ( $dateOver){
        $this->dateOver = $dateOver;
    }

    /**
     * @return \DateTime|null
     */
    public function getDatePay (){
        return $this->datePay;
    }

    /**
     * @param \DateTime|null $datePay
     */
    public function setDatePay ( $datePay){
        $this->datePay = $datePay;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateLastupdate (){
        return $this->dateLastupdate;
    }

    /**
     * @param \DateTime|null $dateLastupdate
     */
    public function setDateLastupdate ( $dateLastupdate){
        $this->dateLastupdate = $dateLastupdate;
    }

    /**
     * @return string|null
     */
    public function getReport (){
        return $this->report;
    }

    /**
     * @param string|null $report
     */
    public function setReport ( $report){
        $this->report = $report;
    }

    /**
     * @return string|null
     */
    public function getSinester (){
        return $this->sinester;
    }

    /**
     * @param string|null $sinester
     */
    public function setSinester ( $sinester){
        $this->sinester = $sinester;
    }

    /**
     * @return string|null
     */
    public function getLocation (){
        return $this->location;
    }

    /**
     * @param string|null $location
     */
    public function setLocation ( $location){
        $this->location = $location;
    }

    /**
     * @return string|null
     */
    public function getDestination (){
        return $this->destination;
    }

    /**
     * @param string|null $destination
     */
    public function setDestination ( $destination){
        $this->destination = $destination;
    }

    /**
     * @return string|null
     */
    public function getCostLeadChangeName (){
        return $this->costLeadChangeName;
    }

    /**
     * @param string|null $costLeadChangeName
     */
    public function setCostLeadChangeName ( $costLeadChangeName){
        $this->costLeadChangeName = $costLeadChangeName;
    }

    /**
     * @return int|null
     */
    public function getDistance (){
        return $this->distance;
    }

    /**
     * @param int|null $distance
     */
    public function setDistance ( $distance){
        $this->distance = $distance;
    }

    /**
     * @return string|null
     */
    public function getCostDistance (){
        return $this->costDistance;
    }

    /**
     * @param string|null $costDistance
     */
    public function setCostDistance ( $costDistance){
        $this->costDistance = $costDistance;
    }

    /**
     * @return int|null
     */
    public function getGuardDay (){
        return $this->guardDay;
    }

    /**
     * @param int|null $guardDay
     */
    public function setGuardDay ( $guardDay){
        $this->guardDay = $guardDay;
    }

    /**
     * @return string|null
     */
    public function getCostGuard (){
        return $this->costGuard;
    }

    /**
     * @param string|null $costGuard
     */
    public function setCostGuard ( $costGuard){
        $this->costGuard = $costGuard;
    }

    /**
     * @return string|null
     */
    public function getCostExpired (){
        return $this->costExpired;
    }

    /**
     * @param string|null $costExpired
     */
    public function setCostExpired ( $costExpired){
        $this->costExpired = $costExpired;
    }

    /**
     * @return int|null
     */
    public function getExtraLoad (){
        return $this->extraLoad;
    }

    /**
     * @param int|null $extraLoad
     */
    public function setExtraLoad ( $extraLoad){
        $this->extraLoad = $extraLoad;
    }

    /**
     * @return string|null
     */
    public function getCostLoad (){
        return $this->costLoad;
    }

    /**
     * @param string|null $costLoad
     */
    public function setCostLoad ( $costLoad){
        $this->costLoad = $costLoad;
    }

    /**
     * @return int|null
     */
    public function getHandling (){
        return $this->handling;
    }

    /**
     * @param int|null $handling
     */
    public function setHandling ( $handling){
        $this->handling = $handling;
    }

    /**
     * @return string|null
     */
    public function getCostHandling (){
        return $this->costHandling;
    }

    /**
     * @param string|null $costHandling
     */
    public function setCostHandling ( $costHandling){
        $this->costHandling = $costHandling;
    }

    /**
     * @return string|null
     */
    public function getArmor (){
        return $this->armor;
    }

    /**
     * @param string|null $armor
     */
    public function setArmor ( $armor){
        $this->armor = $armor;
    }

    /**
     * @return string|null
     */
    public function getDolly (){
        return $this->dolly;
    }

    /**
     * @param string|null $dolly
     */
    public function setDolly ( $dolly){
        $this->dolly = $dolly;
    }

    /**
     * @return string|null
     */
    public function getAmount (){
        return $this->amount;
    }

    /**
     * @param string|null $amount
     */
    public function setAmount ( $amount){
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getAmountaux (){
        return $this->amountaux;
    }

    /**
     * @param string|null $amountaux
     */
    public function setAmountaux ( $amountaux){
        $this->amountaux = $amountaux;
    }

    /**
     * @return string|null
     */
    public function getBill (){
        return $this->bill;
    }

    /**
     * @param string|null $bill
     */
    public function setBill ( $bill){
        $this->bill = $bill;
    }

    /**
     * @return string|null
     */
    public function getBillfilepdf (){
        return $this->billfilepdf;
    }

    /**
     * @param string|null $billfilepdf
     */
    public function setBillfilepdf ( $billfilepdf){
        $this->billfilepdf = $billfilepdf;
    }

    /**
     * @return string|null
     */
    public function getBillfilexml (){
        return $this->billfilexml;
    }

    /**
     * @param string|null $billfilexml
     */
    public function setBillfilexml ( $billfilexml){
        $this->billfilexml = $billfilexml;
    }

    /**
     * @return string|null
     */
    public function getTaxValue (){
        return $this->taxValue;
    }

    /**
     * @param string|null $taxValue
     */
    public function setTaxValue ( $taxValue){
        $this->taxValue = $taxValue;
    }

    /**
     * @return string|null
     */
    public function getComent (){
        return $this->coment;
    }
    /**
     * @param string|null $coment
     */
    public function setComent ( $coment) {
        $this->coment = $coment;
    }
    public function __toString () {
        // TODO: Implement __toString() method.
        return $this->getId()." / ".$this->getDateCapture()->format('Y/m/d H:i:s');
    }
    public $timediff;
    /**
     * @return integer
     */
    public function getTimediff () {
        $datetime1 = $this->dateCapture;
        $datetime2 = $this->dateContact;
        $interval = $datetime1->diff($datetime2);
        //var_dump($interval);
        return intval($interval->i);
    }

}
