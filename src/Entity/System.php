<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * System
 *
 * @ORM\Table(name="system", indexes={@ORM\Index(name="fk_system_country1_idx", columns={"default_country_code"})})
 * @ORM\Entity
 */
class System
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_path", type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var string|null
     *
     * @ORM\Column(name="defaultProfileImage", type="string", length=255, nullable=true)
     */
    private $defaultprofileimage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="defaultAsistanceVehicleimage", type="string", length=255, nullable=true)
     */
    private $defaultasistancevehicleimage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="defaultLogoImage", type="string", length=255, nullable=true)
     */
    private $defaultlogoimage;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="default_country_code", referencedColumnName="code")
     * })
     */
    private $defaultCountryCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId( $id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getImagePath(){
		return $this->imagePath;
	}

	/**
	 * @param string|null $imagePath
	 */
	public function setImagePath($imagePath) {
		$this->imagePath = $imagePath;
	}

	/**
	 * @return string|null
	 */
	public function getFilePath(){
		return $this->filePath;
	}

	/**
	 * @param string|null $filePath
	 */
	public function setFilePath($filePath) {
		$this->filePath = $filePath;
	}

	/**
	 * @return string|null
	 */
	public function getDefaultprofileimage(){
		return $this->defaultprofileimage;
	}

	/**
	 * @param string|null $defaultprofileimage
	 */
	public function setDefaultprofileimage($defaultprofileimage){
		$this->defaultprofileimage = $defaultprofileimage;
	}

	/**
	 * @return string|null
	 */
	public function getDefaultasistancevehicleimage(){
		return $this->defaultasistancevehicleimage;
	}

	/**
	 * @param string|null $defaultasistancevehicleimage
	 */
	public function setDefaultasistancevehicleimage($defaultasistancevehicleimage){
		$this->defaultasistancevehicleimage = $defaultasistancevehicleimage;
	}

	/**
	 * @return string|null
	 */
	public function getDefaultlogoimage(){
		return $this->defaultlogoimage;
	}

	/**
	 * @param string|null $defaultlogoimage
	 */
	public function setDefaultlogoimage($defaultlogoimage){
		$this->defaultlogoimage = $defaultlogoimage;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDate(){
		return $this->date;
	}

	/**
	 * @param \DateTime|null $date
	 */
	public function setDate($date){
		$this->date = $date;
	}

	/**
	 * @return \Country
	 */
	public function getDefaultCountryCode(){
		return $this->defaultCountryCode;
	}

	/**
	 * @param \Country $defaultCountryCode
	 */
	public function setDefaultCountryCode($defaultCountryCode){
		$this->defaultCountryCode = $defaultCountryCode;
	}



}
