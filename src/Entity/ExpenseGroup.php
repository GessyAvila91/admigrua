<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpenseGroup
 *
 * @ORM\Table(name="expense_group", uniqueConstraints={@ORM\UniqueConstraint(name="code_UNIQUE", columns={"code"})})
 * @ORM\Entity
 */
class ExpenseGroup
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false, options={"fixed"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

	/**
	 * @return string
	 */
	public function getCode(){
		return $this->code;
	}

	/**
	 * @param string $code
	 */
	public function setCode($code){
		$this->code = $code;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription($description){
		$this->description = $description;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreationDate(){
		return $this->creationDate;
	}

	/**
	 * @param \DateTime|null $creationDate
	 */
	public function setCreationDate($creationDate){
		$this->creationDate = $creationDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdatedDate(){
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime|null $updatedDate
	 */
	public function setUpdatedDate($updatedDate){
		$this->updatedDate = $updatedDate;
	}


	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}

}
