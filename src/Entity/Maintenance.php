<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Maintenance
 *
 * @ORM\Table(name="maintenance", indexes={@ORM\Index(name="fk_maintenance_expense1_idx", columns={"expense_id"}), @ORM\Index(name="fk_maintenance_assistanse_vehicle_idx", columns={"assistance_vehicle_id"})})
 * @ORM\Entity
 */
class Maintenance  {
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true, options={"comment"="Fecha del Mantenimiento"})
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, options={"comment"="descripcion de servicio de mantenimeinto"})
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=true)
     */
    private $timeStamp;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \Expense
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expense")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expense_id", referencedColumnName="id")
     * })
     */
    private $expense;

    /**
     * @return \DateTime|null
     */
    public function getDate (){
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate ($date){
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getDescription (){
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription ($description){
        $this->description = $description;
    }

    /**
     * @return \DateTime|null
     */
    public function getTimeStamp (){
        return $this->timeStamp;
    }

    /**
     * @param \DateTime|null $timeStamp
     */
    public function setTimeStamp ($timeStamp){
        $this->timeStamp = $timeStamp;
    }

    /**
     * @return \AssistanceVehicle
     */
    public function getAssistanceVehicle (){
        return $this->assistanceVehicle;
    }

    /**
     * @param \AssistanceVehicle $assistanceVehicle
     */
    public function setAssistanceVehicle ($assistanceVehicle){
        $this->assistanceVehicle = $assistanceVehicle;
    }

    /**
     * @return \Expense
     */
    public function getExpense (){
        return $this->expense;
    }

    /**
     * @param \Expense $expense
     */
    public function setExpense ( $expense){
        $this->expense = $expense;
    }

}
