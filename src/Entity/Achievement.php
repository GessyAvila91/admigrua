<?php
/**
 * *********************************************************************
 * Usuario: Gessy
 * Date: 2019/7/16
 * File: Achievement.php
 * path: C:/xampp/htdocs/www/Admigrua2/src/Entity/Achievement.php
 * project: Admigrua2
 * File: Achievement.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Achievement
 *
 * @ORM\Table(name="achievement", indexes={@ORM\Index(name="fk_achievement_icon1_idx", columns={"icon_id"})})
 * @ORM\Entity
 */
class Achievement  {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="release_date", type="datetime", nullable=true)
     */
    private $releaseDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration_days", type="integer", nullable=true)
     */
    private $durationDays;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="condition_query", type="string", length=255, nullable=true)
     */
    private $conditionQuery;

//    /**
//     * @var \IconAdmin
//     *
//     * @ORM\ManyToOne(targetEntity="IconAdmin")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="icon_id", referencedColumnName="id")
//     * })
//     */
//    private $icon;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="achievement")
     */
    private $userCode;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCode = new \Doctrine\Common\Collections\ArrayCollection();
    }

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId( $id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName( $name){
		$this->name = $name;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getReleaseDate(){
		return $this->releaseDate;
	}

	/**
	 * @param \DateTime|null $releaseDate
	 */
	public function setReleaseDate($releaseDate){
		$this->releaseDate = $releaseDate;
	}

	/**
	 * @return int|null
	 */
	public function getPoints(){
		return $this->points;
	}

	/**
	 * @param int|null $points
	 */
	public function setPoints( $points){
		$this->points = $points;
	}

	/**
	 * @return int|null
	 */
	public function getDurationDays(){
		return $this->durationDays;
	}

	/**
	 * @param int|null $durationDays
	 */
	public function setDurationDays( $durationDays){
		$this->durationDays = $durationDays;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription( $description){
		$this->description = $description;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreationDate(){
		return $this->creationDate;
	}

	/**
	 * @param \DateTime|null $creationDate
	 */
	public function setCreationDate($creationDate){
		$this->creationDate = $creationDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdatedDate(){
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime|null $updatedDate
	 */
	public function setUpdatedDate($updatedDate){
		$this->updatedDate = $updatedDate;
	}

	/**
	 * @return string|null
	 */
	public function getConditionQuery(){
		return $this->conditionQuery;
	}

	/**
	 * @param string|null $conditionQuery
	 */
	public function setConditionQuery($conditionQuery){
		$this->conditionQuery = $conditionQuery;
	}

	/**
	 * @return \Icon
	 */
	public function getIcon(){
		return $this->icon;
	}

	/**
	 * @param \Icon $icon
	 */
	public function setIcon($icon){
		$this->icon = $icon;
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getUserCode(){
		return $this->userCode;
	}

	/**
	 * @param \Doctrine\Common\Collections\Collection $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}



}
