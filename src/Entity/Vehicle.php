<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicle
 *
 * @ORM\Table(name="vehicle", indexes={@ORM\Index(name="fk_vehicle_brand1_idx", columns={"brand_id"}), @ORM\Index(name="fk_vehicle_type_vehicle_idx", columns={"type_vehicle_id"})})
 * @ORM\Entity
 */
class Vehicle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true, options={"comment"="modelo Ejm {corolla,camry}"})
     */
    private $model;

    /**
     * @var string|null
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * @var \TypeVehicle
     *
     * @ORM\ManyToOne(targetEntity="TypeVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_vehicle_id", referencedColumnName="id")
     * })
     */
    private $typeVehicle;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getModel(){
		return $this->model;
	}

	/**
	 * @param string|null $model
	 */
	public function setModel($model){
		$this->model = $model;
	}

	/**
	 * @return string|null
	 */
	public function getSlug(){
		return $this->slug;
	}

	/**
	 * @param string|null $slug
	 */
	public function setSlug($slug){
		$this->slug = $slug;
	}

	/**
	 * @return \Brand
	 */
	public function getBrand(){
		return $this->brand;
	}

	/**
	 * @param \Brand $brand
	 */
	public function setBrand( $brand){
		$this->brand = $brand;
	}

	/**
	 * @return \TypeVehicle
	 */
	public function getTypeVehicle(){
		return $this->typeVehicle;
	}

	/**
	 * @param \TypeVehicle $typeVehicle
	 */
	public function setTypeVehicle($typeVehicle){
		$this->typeVehicle = $typeVehicle;
	}

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getSlug();
	}

}
