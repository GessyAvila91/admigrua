<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Brand
 *
 * @ORM\Table(name="brand")
 * @ORM\Entity
 */
class Brand  {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment"="Nombre de la marca Ejm {Toyota,Alfa-Romero}"})
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @param string|null $slug
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
	}

	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getName();
	}

}
