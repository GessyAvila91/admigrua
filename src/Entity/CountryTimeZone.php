<?php
/**
 * *********************************************************************
 * Usuario geavila
 * Date 2019/9/25
 * File CountryTimeZone.php
 * path C/xampp/htdocs/www/Admigrua2/src/Entity/CountryTimeZone.php
 * project Admigrua2
 * File CountryTimeZone.php
 * *********************************************************************
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="CountryTimeZone")
 */
class CountryTimeZone {
    /**
     * @var string|null
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="countrycode", type="string", length=2, nullable=false, options={"fixed"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $countrycode;
    /**
     * @var string|null
     * @ORM\Column(name="countrydecimal", type="string", length=255, nullable=true)
     */
    private $countrydecimal;
    /**
     * @var string|null
     * @ORM\Column(name="countryhexadecimal", type="string", length=255, nullable=true)
     */
    private $countryhexadecimal;
    /**
     * @var string|null
     * @ORM\Column(name="zonename", type="string", length=35, nullable=true)
     */
    private $zonename;
    /**
     * @var string|null
     * @ORM\Column(name="abbreviartion", type="string", length=6, nullable=true)
     */
    private $abbreviartion;
    /**
     * @var string|null
     * @ORM\Column(name="time_start", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $timeStart;
    /**
     * @var int|null
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var bool|null
     * @ORM\Column(name="dst", type="boolean", nullable=true, options={"default"="1"})
     */
    private $dst;

    /**
     * @return string|null
     */
    public function getName ()  {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName ( $name){
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCountrycode ()  {
        return $this->countrycode;
    }

    /**
     * @param string|null $countrycode
     */
    public function setCountrycode ( $countrycode){
        $this->countrycode = $countrycode;
    }

    /**
     * @return string|null
     */
    public function getCountrydecimal ()  {
        return $this->countrydecimal;
    }

    /**
     * @param string|null $countrydecimal
     */
    public function setCountrydecimal ( $countrydecimal){
        $this->countrydecimal = $countrydecimal;
    }

    /**
     * @return string|null
     */
    public function getCountryhexadecimal ()  {
        return $this->countryhexadecimal;
    }

    /**
     * @param string|null $countryhexadecimal
     */
    public function setCountryhexadecimal ( $countryhexadecimal){
        $this->countryhexadecimal = $countryhexadecimal;
    }

    /**
     * @return string|null
     */
    public function getZonename ()  {
        return $this->zonename;
    }

    /**
     * @param string|null $zonename
     */
    public function setZonename ( $zonename){
        $this->zonename = $zonename;
    }

    /**
     * @return string|null
     */
    public function getAbbreviartion ()  {
        return $this->abbreviartion;
    }

    /**
     * @param string|null $abbreviartion
     */
    public function setAbbreviartion ( $abbreviartion){
        $this->abbreviartion = $abbreviartion;
    }

    /**
     * @return string|null
     */
    public function getTimeStart ()  {
        return $this->timeStart;
    }

    /**
     * @param string|null $timeStart
     */
    public function setTimeStart ( $timeStart){
        $this->timeStart = $timeStart;
    }

    /**
     * @return int|null
     */
    public function getGmtOffset () {
        return $this->gmtOffset;
    }

    /**
     * @param int|null $gmtOffset
     */
    public function setGmtOffset ($gmtOffset){
        $this->gmtOffset = $gmtOffset;
    }

    /**
     * @return bool|null
     */
    public function getDst (){
        return $this->dst;
    }

    /**
     * @param bool|null $dst
     */
    public function setDst ($dst){
        $this->dst = $dst;
    }
    
}
/*
 * https://logos-download.com/wp-content/uploads/2019/06/Dota_2_Logo-700x541.png
 * 1  Blue Eyes Withe Dragon (ygo)
 * 2  Red Eyes Black Dragon  (ygo)
 * 3  Smaug                  (JRR Tolking)
 * 4  Chimuelo               (Entrenando a tu dragon)
 * 5  Jakiro                 (Dota y Dota2)
 * 6  Ghidora                (Godzilla)
 * 7  Brionac                (ygo)
 * 8  Sindragosa             (WoW)
 * 9  Alamuerte              (WoW)
 * 10 Fujur                  (Hisotia sin fin)
 * */