<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expense
 *
 * @ORM\Table(name="expense", indexes={@ORM\Index(name="fk_expense_user1_idx", columns={"user_code"}), @ORM\Index(name="fk_expense_expense_group1_idx", columns={"group_code"}), @ORM\Index(name="fk_expense_assistance_vehicle1_idx", columns={"assistance_vehicle_id"})})
 * @ORM\Entity
 */
class Expense
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=true)
     */
    private $timeStamp;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \ExpenseGroup
     *
     * @ORM\ManyToOne(targetEntity="ExpenseGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_code", referencedColumnName="code")
     * })
     */
    private $groupCode;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_code", referencedColumnName="code")
     * })
     */
    private $userCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getAmount(){
		return $this->amount;
	}

	/**
	 * @param string|null $amount
	 */
	public function setAmount($amount){
		$this->amount = $amount;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDate(){
		return $this->date;
	}

	/**
	 * @param \DateTime|null $date
	 */
	public function setDate($date){
		$this->date = $date;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription($description){
		$this->description = $description;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getTimeStamp(){
		return $this->timeStamp;
	}

	/**
	 * @param \DateTime|null $timeStamp
	 */
	public function setTimeStamp($timeStamp){
		$this->timeStamp = $timeStamp;
	}

	/**
	 * @return \AssistanceVehicle
	 */
	public function getAssistanceVehicle(){
		return $this->assistanceVehicle;
	}

	/**
	 * @param \AssistanceVehicle $assistanceVehicle
	 */
	public function setAssistanceVehicle($assistanceVehicle){
		$this->assistanceVehicle = $assistanceVehicle;
	}

	/**
	 * @return \ExpenseGroup
	 */
	public function getGroupCode(){
		return $this->groupCode;
	}

	/**
	 * @param \ExpenseGroup $groupCode
	 */
	public function setGroupCode($groupCode){
		$this->groupCode = $groupCode;
	}

	/**
	 * @return \User
	 */
	public function getUserCode(){
		return $this->userCode;
	}

	/**
	 * @param \User $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}


	public function __toString() {
		// TODO: Implement __toString() method.
		return $this->getDescription();
	}

}
