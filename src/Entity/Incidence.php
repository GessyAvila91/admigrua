<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidence
 *
 * @ORM\Table(name="incidence", indexes={@ORM\Index(name="fk_incidence_expense1_idx", columns={"expense_id"}), @ORM\Index(name="fk_incidence_assistance_vehicle1_idx", columns={"assistance_vehicle_id"})})
 * @ORM\Entity
 */
class Incidence  {
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=true)
     */
    private $timeStamp;

    /**
     * @var \AssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="AssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $assistanceVehicle;

    /**
     * @var \Expense
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expense")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expense_id", referencedColumnName="id")
     * })
     */
    private $expense;

    /**
     * @return \DateTime|null
     */
    public function getDate (){
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate ($date){
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getDescription (){
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription ($description){
        $this->description = $description;
    }

    /**
     * @return \DateTime|null
     */
    public function getTimeStamp (){
        return $this->timeStamp;
    }

    /**
     * @param \DateTime|null $timeStamp
     */
    public function setTimeStamp ($timeStamp){
        $this->timeStamp = $timeStamp;
    }

    /**
     * @return \AssistanceVehicle
     */
    public function getAssistanceVehicle (){
        return $this->assistanceVehicle;
    }

    /**
     * @param \AssistanceVehicle $assistanceVehicle
     */
    public function setAssistanceVehicle ($assistanceVehicle){
        $this->assistanceVehicle = $assistanceVehicle;
    }

    /**
     * @return \Expense
     */
    public function getExpense (){
        return $this->expense;
    }

    /**
     * @param \Expense $expense
     */
    public function setExpense ($expense){
        $this->expense = $expense;
    }



}
