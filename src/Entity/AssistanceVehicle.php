<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AssistanceVehicle
 *
 * @ORM\Table(name="assistance_vehicle", indexes={@ORM\Index(name="fk_assistance_vehicle_type_assistance_vehicle1_idx", columns={"type_assistance_vehicle_id"}), @ORM\Index(name="fk_assistance_vehicle_user1_idx", columns={"user_code"})})
 * @ORM\Entity
 */

class AssistanceVehicle {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plate", type="string", length=10, nullable=true, options={"comment"="Año o version del vehiculo"})
     */
    private $plate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @var string|null
     *
     * @ORM\Column(name="brand", type="string", length=255, nullable=true, options={"comment"="marca del vehiculo"})
     */
    private $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true, options={"comment"="modelo del vehiculo"})
     */
    private $model;

    /**
     * @var string|null
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var string|null
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $active;

    /**
     * @var \TypeAssistanceVehicle
     *
     * @ORM\ManyToOne(targetEntity="TypeAssistanceVehicle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_assistance_vehicle_id", referencedColumnName="id")
     * })
     */
    private $typeAssistanceVehicle;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_code", referencedColumnName="code")
     * })
     */
    private $userCode;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getPlate(){
		return $this->plate;
	}

	/**
	 * @param string|null $plate
	 */
	public function setPlate($plate){
		$this->plate = $plate;
	}

	/**
	 * @return string|null
	 */
	public function getAlias(){
		return $this->alias;
	}

	/**
	 * @param string|null $alias
	 */
	public function setAlias($alias){
		$this->alias = $alias;
	}

	/**
	 * @return string|null
	 */
	public function getBrand(){
		return $this->brand;
	}

	/**
	 * @param string|null $brand
	 */
	public function setBrand($brand){
		$this->brand = $brand;
	}

	/**
	 * @return string|null
	 */
	public function getModel(){
		return $this->model;
	}

	/**
	 * @param string|null $model
	 */
	public function setModel($model){
		$this->model = $model;
	}

	/**
	 * @return string|null
	 */
	public function getVersion(){
		return $this->version;
	}

	/**
	 * @param string|null $version
	 */
	public function setVersion($version){
		$this->version = $version;
	}

	/**
	 * @return string|null
	 */
	public function getPhoto(){
		return $this->photo;
	}

	/**
	 * @param string|null $photo
	 */
	public function setPhoto($photo){
		$this->photo = $photo;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreationDate(){
		return $this->creationDate;
	}

	/**
	 * @param \DateTime|null $creationDate
	 */
	public function setCreationDate( $creationDate){
		$this->creationDate = $creationDate;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getUpdatedDate(){
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime|null $updatedDate
	 */
	public function setUpdatedDate( $updatedDate){
		$this->updatedDate = $updatedDate;
	}

	/**
	 * @return bool|null
	 */
	public function getActive(){
		return $this->active;
	}

	/**
	 * @param bool|null $active
	 */
	public function setActive($active){
		$this->active = $active;
	}

	/**
	 * @return \TypeAssistanceVehicle
	 */
	public function getTypeAssistanceVehicle(){
		return $this->typeAssistanceVehicle;
	}

	/**
	 * @param \TypeAssistanceVehicle $typeAssistanceVehicle
	 */
	public function setTypeAssistanceVehicle($typeAssistanceVehicle){
		$this->typeAssistanceVehicle = $typeAssistanceVehicle;
	}

	/**
	 * @return \User
	 */
	public function getUserCode(){
		return $this->userCode;
	}

	/**
	 * @param \User $userCode
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}


    public function __toString () {
        // TODO: Implement __toString() method.
        return $this->getAlias();
    }
}
