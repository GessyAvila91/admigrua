

![Gruas 24-7](https://gruas24-7.mx/assets/img/logo-fullcolor.svg)
![Gruas 24-7](https://symfony.com/logos/symfony_black_03.svg)
#AdmiGrua V 0.2.0 Build: Alpha

###Desc
Sistema de control de servicios y gastos 
###test
###Tecnologias

[![php](https://img.shields.io/badge/Sonata-v3.x-blue)](https://github.com/sonata-project/SonataAdminBundle/tree/3.x)
[![php](https://img.shields.io/badge/Symfony-v4.3.9-blue)](https://github.com/symfony/symfony/tree/4.3)
[![php](https://img.shields.io/badge/PHP-%5E7.1.3-informational)](https://github.com/php/php-src/tree/PHP-7.1.3)

####Tabla de contenido  
1. [Servicios](#Servicios)
2. [Vehículos y Vehículos de Asistencia](#Vehículos y Vehículos de Asistencia)
3. [Clientes](#Clientes)
4. [Usuarios](#Usuarios)
5. [Facturas](#Facturas)
6. [Gastos](#Gastos) 
7. [Pagos](#Usuarios)  

## Servicios
- Asistencia
    - Costos
    - Facturacion 
    - Control de tiempos de arribo
    - Insidencias
                  
## Vehículos y Vehículos de Asistencia
- Vehiculos
    - Listado de modelos
    - Caracteristicas
    - categorisacion
- Vehiculos de Asistencia
    - Control de Gastos
    - Fecha de Mantenimientos
    - Consumo de gasolina
    - Incidencias
    - Record de asistencias 
    
## Clientes
- Clientes
    - Control de facturas
    - Cuentas por Cobrar
    - Credito
    
## Usuarios
- Usuarios
    - Control de Acceso por Listado
        - Clientes
            - Facturas
            - Pagos Pendientes
            - Servicios 
        - Empleados
            - Gamification
            - Metas de Desempenio
            - Gestion de Informacion Personal
            - Control de horario de trabajo 
## Facturas
## Gastos  
## Pagos

Dante    